package leetcode;

public class AddTwoNumbers {
    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        ListNode res = new ListNode();
        ListNode pointer = res;
        int tempSum = 0;
        int inMind = 0;
        while (true) {
            if (l1 != null) {
                tempSum += l1.val;
                l1 = l1.next;
            }
            if (l2 != null) {
                tempSum += l2.val;
                l2 = l2.next;
            }
            tempSum = inMind == 1 ? ++tempSum : tempSum;
            inMind = tempSum >= 10 ? tempSum / 10 : 0;

            if (l1 == null && l2 == null) {
                pointer.val = tempSum % 10;
                if (tempSum > 10) {
                    pointer.next = new ListNode(tempSum / 10);
                    break;
                }
            }
            pointer.val = tempSum % 10;
            pointer.next = new ListNode();
            pointer = pointer.next;
            tempSum = 0;
        }

        return res;
    }
}